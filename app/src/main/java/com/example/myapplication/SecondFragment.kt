package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import com.example.myapplication.databinding.FragmentSecondBinding

fun awesome (a: Int, b: Int): Int {
    if (a > b) {
        return a + b
    } else if (a < b) {
        return a - b
    } else {
        return a * b
    }
}

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        val tv = binding.root.findViewById<TextView>(R.id.textview_second)
        var out = ""
        val items = listOf("a", "b", "c")
        for (idx in 50 downTo 15 step 2) {
            when (idx) {
                in 25..33 -> {
                    out += "квадрат числа $idx равен ${idx * idx}\n"
                    }
                in 17..24 -> {
                    out += "куб числа $idx равен ${idx * idx * idx}\n"
                }
                16 -> {
                    out += "16 маленькое\n"
                }
                else -> {
                    out += "число $idx равно $idx\n"
                }
            }
        }
        tv.text = out
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonSecond.setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}